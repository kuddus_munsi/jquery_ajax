<?php
	$filePath = realpath(dirname(__FILE__));
	include_once ($filePath.'/../lib/Database.php');

	class Project
	{
		
		private $db;
		function __construct()
		{
			$this->db = new Database();
		}

		public function getUserName($userName)
		{
			$query  = "select * from user_name where name = '$userName'";
			$result = $this->db->select($query);

			if($userName == ""){
				echo "<span style = 'color:red;'>please enter your name</span>";
				exit();
			}elseif($result){
				echo "<span style = 'color:red;'><b>$userName</b> not available</span>";
				exit();
			}else{
				echo "<span style = 'color:green;'><b>$userName</b>  available</span>";
				exit();

			}
		}


		public function getAutoCompleteSkill($skill)
		{

			$query  = "select * from skill where skill LIKE '%$skill%'";
			$getResult = $this->db->select($query);
			$result = '';
			$result.= '<div class = "skill"><ul>';
			if($getResult != ''){
				while ($data = $getResult->fetch_assoc()) {
					$result.='<li>'.$data['skill'].'</li>';
					}
				}else{
					$result.='<li>Not found</li>';

				}
				$result.= '</ul></div>';
				echo $result;
			

		
	}

	public function autoRefresh($content)
	{
		$query = "INSERT INTO content (body) VALUES ('$content')";
		$result = $this->db->insert($query);
	}


	public function getDataWithAutoRefresh()
	{

			$query  = "select * from content order by id desc";
			$getResult = $this->db->select($query);
			$result = '';
			$result.= '<div class = "refresh"><ul>';
			if($getResult != ''){
				while ($data = $getResult->fetch_assoc()) {
					$result.='<li>'.$data['body'].'</li>';
					}
				}else{
					$result.='<li>Not found</li>';

				}
				$result.= '</ul></div>';
				echo $result;
	}


	public function autoSearch($search)
	{

			$query  = "select * from search where userName LIKE '%$search%'";
			$getResult = $this->db->select($query);
			if($getResult){
				$result = '';
				$result .= '<table class = "tableDesign"><tr>
					<th>User Name</th>
					<th>Name</th>
					<th>Email</th>
				</tr>';
				while ($data = $getResult->fetch_assoc()) {
					$result .= '<tr>
								<td>'.$data['userName'].'</td>
								<td>'.$data['name'].'</td>
								<td>'.$data['email'].'</td>
								</tr>';
				}
				$result .= '</table>';
				echo $result;
			}else{

				echo "data not found";
			}
	}

	public function autoSave($content, $contentID)
	{
		if($contentID != ""){
			$updateQuery = "UPDATE save SET content = '$content' where id = '$contentID'";
			$result = $this->db->update($updateQuery);
		}else{
			$insertQuery = "INSERT INTO save (content , status) VALUES ('$content','draft')";
			$result = $this->db->insert($insertQuery);
			$inserID = $this->db->link->insert_id;
			echo $inserID;
			exit();
		}
	}
}

?>