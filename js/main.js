$(document).ready(function(){

	//check user name 
	$('#userName').blur(function(){

		var userName = $(this).val();
		$.ajax({

			url:"check/checkUserNamee.php",
			method:"POST",
			data:{userName:userName},
			dataType:"text",
			success:function(data){
				$('#userStatus').html(data);

			}
		});
	});


	//Auto complete by ajax and jquery

	$('#skill').keyup(function(){
		var skill = $(this).val();
		if(skill != ''){
				$.ajax({

					url:"check/autoComplete.php",
					method:"POST",
					data:{skill:skill},
					success:function(data){
						$('#skillStatus').fadeIn();
						$('#skillStatus').html(data);

					}
				});
			}


			$(document).on('click', 'li', function(){
				$('#skill').val($(this).text());
					$('#skillStatus').fadeOut();
			});
	});

	//Showing password jquery
	$("#showpass").on('click', function(){

		var pass = $("#password");
		var fieldType = pass.attr("type");
		if(fieldType == 'password'){
			pass.attr('type', 'text');
			$(this).text("Hide password");
		}else{
			pass.attr('type', 'password');
			$(this).text("show password");
		}


	});

	//Auto Refresh ajax by JQuery

	$("#autoRefresh").click(function(){

		var body = $('#body').val();
		if($.trim(body) != ''){
				$.ajax({

					url:"check/autoRefresh.php",
					method:"POST",
					data:{body:body},
					success:function(data){
						$('#body').val("");

					}
				});
				return false;
		}
	});

	setInterval(function(){
		$('#contentStatus').load('check/getContent.php').fadeIn('slow');

	},1000);


	//Auto search ajax by JQuery

	$('#autoSearch').keyup(function(){

		var search = $(this).val();
		if(search != ''){
				$.ajax({

					url:"check/autoSearch.php",
					method:"POST",
					data:{search:search},
					success:function(data){
						$('#searchStatus').html(data);

					}
				});
			}else{
				$('#searchStatus').html(" ");
			}

	});



	function autoSave(){

		var content = $('#content').val();
		var contentID = $('#contentID').val();

		if(content != ""){
				$.ajax({

					url:"check/autoSave.php",
					method:"POST",
					data:{content:content,contentID:contentID},
					type:"text",
					success:function(data){
						if(data != ''){
							$('#contentID').val(data);
						}
						$('#saveStatus').text("post save as draft...");
						setInterval(function(){
							$('#saveStatus').text(" ");
						},2000);
					}
				});
		}
	}

	setInterval(function(){
		autoSave()
	},10000);

});